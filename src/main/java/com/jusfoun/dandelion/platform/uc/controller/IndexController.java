package com.jusfoun.dandelion.platform.uc.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;

import com.jusfoun.dandelion.platform.uc.model.PtUser;
import com.jusfoun.dandelion.platform.uc.service.IUserService;
import com.jusfoun.dandelion.platform.uc.util.StringUtil;

//@RestController
@Controller
public class IndexController {
	
	@Autowired
	private IUserService userService;

	@RequestMapping(value = "/home")
	String home(Model model) {
		PtUser record = new PtUser();
		record.setId(StringUtil.genUUID());
		record.setName("张汉青");
		record.setPassword(StringUtil.encoderByMd5("11"));
		userService.insert(record);
		return "home";
	}

	@RequestMapping(value = "/aa")
	String index(Model model) {
		PtUser record = new PtUser();
		record.setId(StringUtil.genUUID());
		record.setName("张汉青");
		record.setPassword(StringUtil.encoderByMd5("11"));
		return "index";
	}

}
