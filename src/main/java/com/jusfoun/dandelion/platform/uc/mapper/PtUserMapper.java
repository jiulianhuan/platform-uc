package com.jusfoun.dandelion.platform.uc.mapper;

import com.jusfoun.dandelion.platform.uc.model.PtUser;
import com.jusfoun.dandelion.platform.uc.model.PtUserExample;
import java.util.List;
import org.apache.ibatis.annotations.Param;

public interface PtUserMapper {
    int countByExample(PtUserExample example);

    int deleteByExample(PtUserExample example);

    int deleteByPrimaryKey(String id);

    int insert(PtUser record);

    int insertSelective(PtUser record);

    List<PtUser> selectByExample(PtUserExample example);

    PtUser selectByPrimaryKey(String id);

    int updateByExampleSelective(@Param("record") PtUser record, @Param("example") PtUserExample example);

    int updateByExample(@Param("record") PtUser record, @Param("example") PtUserExample example);

    int updateByPrimaryKeySelective(PtUser record);

    int updateByPrimaryKey(PtUser record);
}