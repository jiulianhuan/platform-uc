package com.jusfoun.dandelion.platform.uc.service;

import com.jusfoun.dandelion.platform.uc.model.PtUser;

public interface IUserService {
	/**
	 * 新增用户
	 * @param record
	 */
	void insert(PtUser record);

}
