package com.jusfoun.dandelion.platform.uc.service.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.jusfoun.dandelion.platform.uc.mapper.PtUserMapper;
import com.jusfoun.dandelion.platform.uc.model.PtUser;
import com.jusfoun.dandelion.platform.uc.service.IUserService;
@Service
public class UserService implements IUserService {
	@Autowired
    private PtUserMapper ptUserMapper;

	@Override
	public void insert(PtUser record) {
		ptUserMapper.insert(record);
	}
}
